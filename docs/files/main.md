[Home](../../README.md)

# Main Screen

- Contains a map view and a marker indicating the current device location

## Zero State
![splash](../screens/location.png)

## Notification
![splash](../screens/notification.png)
