# The Location Tracker
You are going to create a fairly simple GPS tracker Android application.

## Task 1 - UI imlementation
- Create Activity classes accordingly based on the screen designs provided below:
    - [Login](docs/files/login.md)
    - [Register](docs/files/register.md)
    - [Main](docs/files/main.md)
- Note: the screens are just for reference, you are free to make your own design

## Task 2 - API Integration
Integrate ``Login`` and ``Register`` API to Login and Register screens respectively.

### Standard Rules
- All responses is in ``JSON`` format
- Use standard HTTP status code to determine if a response is successful, eg ``200`` is successful
- Base URL: ``http://198.211.112.145:8001``
- Example request call: ``http://198.211.112.145:8001/api/register``

### API Endpoints

#### Register a new account
- ``POST /api/register``
- #### Request Payload
```json
{
    "username": "myusername",
    "password": "900150983cd24fb0d6963f7d28e17f72",
    "first_name": "Lucy",
    "last_name": "Chooi"
}
```
- #### Response
```json
{
  "success": true,
  "status": 200,
  "message": "You have successfully registered an account.",
  "data": {
    "user": {
      "id": 3,
      "username": "myusername",
      "password": "900150983cd24fb0d6963f7d28e17f72",
      "created_at": "2017-02-16T17:35:07.702Z",
      "updated_at": "2017-02-16T17:35:07.702Z",
      "first_name": "Lucy",
      "last_name": "Chooi"
    }
  }
}
```

#### Login
- ``POST /api/login``
- #### Request Payload
```json
{
    "username": "myusername",
    "password": "900150983cd24fb0d6963f7d28e17f72"
}
```
- #### Response
```json
{
  "success": true,
  "status": 200,
  "message": "Login successfully, welcome",
  "data": {
    "token": "32dc5a58-6174-4a64-be9c-d4f663fb4d8c"
  }
}
```

#### Location Heartbeat
- API to send device's latest GPS latitude and longitude coordinates to the server
- ``POST /api/heartbeat``
- #### Response
```json
{
    "lat": 763.656677037990,
    "lng": 7.67555576262133
}
```



## Task 3 - Location Tracking Listener

### 1. Location change detection
The app must be able to detect the device's GPS location change.
When location changes, get the current location coordinates (lat, long) and update the server via ``/api/heartbeat`` API.

### 2. Heartbeat in foreground
- When the app is in the foreground (ie. UI visible to user), do the following:
    - The app must be able to send the current GPS coordinates to the server every 10 seconds, and show, or update, a marker on the Google Map View based on the GPS coordinates.
    - Stop any background service (Refer to task #3 below)

### 3. Heartbeat in background
- When the app is in the background, do the following:
    - Start an Android Service to send current GPS coordinates to the server every 10 seconds.
    - If the location has changed, show a Notification like in the [screenshot](docs/files/main.md), the notification must:
      - Show the current lat, long values
      - Have 2 buttons: ``Close`` and ``Open``. Cliking ``Close`` will dismiss the notification, ``Open`` will open up the application.
      - Note: The screenshot is for reference, you are free to make your own design.